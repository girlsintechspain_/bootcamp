# **BootCamp CSS y HTML con GIT → ¡HAZ TU CV DIGITAL!**

**Fecha:** 19/04 al 22/04

Agregamos cada una de las presentaciones que se hicieron en el **BootCamp** dividimos los Días en dos partes, la primera parte con **Emplea Tech** y la segunda parte con el **BootCamp**

### **Día** 1
#### **Emplea Tech**

<img src="img/ms.png" width="100px" /> Proceso de selección de perfiles técnicos en Making Science por **Montse Muñoz**

<img src="img/GIT.jpg" width="30px" /> Presentación GIT por [Lucía Manzano](https://www.linkedin.com/in/luciarmanzanogomez/)


[![feria](http://img.youtube.com/vi/Sn04_W_5r1Q/0.jpg)](https://youtu.be/Sn04_W_5r1Q)

#### **BootCamp**
Aquí los nombres de cada una de las Charlas

<img src="img/GIT.jpg" width="30px" />  <**HTML**> es solo el comienzo

<img src="img/europa.svg" width="80px"/> CSS: Nice to meet you! por la Europea

👩‍💻 **Práctica del CV** 👩‍💻

[![bootcamp](http://img.youtube.com/vi/JUsllB-ew0A/0.jpg)](https://youtu.be/JUsllB-ew0A)

-----
### **Día** 2
#### **Emplea Tech**

<img src="img/adevinta.png" width="150px"/> Cómo darlo todo en una entrevista  y no morir en el intento por  **Cristina Edo García & Clara Xufré Luna**

<img src="img/youcanbook.png" width="120px"/>  How to get a tech job anywhere in the world por **Bridget Harris**

[![feria](http://img.youtube.com/vi/o2BxhItg3xI/0.jpg)](https://youtu.be/o2BxhItg3xI)
#### **BootCamp**

<img src="img/ms.png" width="100px"/> **Introducción al CSS: estilo, clase, hashtags y otros puntos**

<img src="img/youcanbook.png" width="120px"/> **Hackeando sitios web con HTML y CSS**

<img src="img/GIT.jpg" width="20px" /> 👩‍💻 **Práctica del CV** 👩‍💻

[![bootcamp](http://img.youtube.com/vi/A9D_yHbcOFY/0.jpg)](https://youtu.be/A9D_yHbcOFY)

-----
### **Día** 3
#### **Emplea Tech**

<img src="img/aws.png" width="60px"/> Únete a Amazon por **Valery de Abreu & Teresa Aparicio**

<img src="img/europa.svg" width="80px"/> Salidas profesionales como Desarrolladores web por **Ana del Valle Corrales**

[![feria](http://img.youtube.com/vi/YBmoag1l3l4/0.jpg)](https://youtu.be/YBmoag1l3l4)

#### **BootCamp**

<img src="img/aws.png" width="60px"/> **Introducción a html y JavaScript: mi primera web interactiva** por AWS

<img src="img/eventbrite.png" width="60px"/> El código también puede ser creativo por Eventbrite

<img src="img/GIT.jpg" width="20px" /> 👩‍💻 **Práctica del CV** 👩‍💻

[![bootcamp](http://img.youtube.com/vi/6D_axrWivWY/0.jpg)](https://youtu.be/6D_axrWivWY)

-----
### **Día** 4
#### **Emplea Tech**
<img src="img/codehouse.png" width="60px"/> Resalta tu CV para encontrar tu primer empleo IT por **Lixmar Gómez**

<img src="img/GIT.jpg" width="30px"/> Cuenta reto y promo Hackathon por [Francis Santiago](https://www.linkedin.com/in/sancfc/)

[![feria](http://img.youtube.com/vi/hXEbBba3Fig/0.jpg)](https://youtu.be/hXEbBba3Fig)

#### **BootCamp**

<img src="img/dondominio.png" width="60px" /> **La base de tu Identidad Digital: dominios, hosting y SSL** por Don Dominio

<img src="img/GIT.jpg" width="30px" /> **Mis primeros pasos con GIT** por [Francis Santiago](https://www.linkedin.com/in/sancfc/)

<img src="img/GIT.jpg" width="20px" />👩‍💻 **Preguntas y respuestas** 👩‍💻

[![bootcamp](http://img.youtube.com/vi/1qgSV9NN-Ik/0.jpg)](https://youtu.be/1qgSV9NN-Ik)

> Algunas de las presentaciones pueden encontrarla en Presentaciones/<**Patrocinador**>

-----
### Gracias a nuestros patrocinadores y participantes ¡Nos vemos el año próximo! o en el Hackathon (**19/10**)
![gracias](https://i.pinimg.com/originals/27/f7/f5/27f7f51b18228257c2f438944ea4ede5.gif)
